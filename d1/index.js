/*
What is a client?
	A client is an application which creates request for RESOURCES from a server. A client will trigger an action, in the web development context through a URL and wait for the response of the server

What is a server?
	A server is able to host and deliver Resources requested by a client. In fact, a single server can handle multiple clients

What is Node.js?
	Node.js is a runtime environment which allows us to create/develop backend/server-side applications with JS because by default, JS was conceptualized solely to the front end.
	Runtime Environment - is the environment in which a program or application is executed

	Runtime environment is the environment where an app can 

Why is Node.js Popular?
	Performance - node.js is one of the most performing environment for creating backend applications with JS

	Familiarity - since node.js is built and uses JS as its language, it is very familiar for most developers

	NPM - Node Package Managers - is the largest registry for node packages
	Packages - are bits of programs, methods, functions, codes that greatly help in the development of an application
	
*/


const http = require("http");

// creates a variable "port" to store the port number
const port = 4000

// Creates a variable 'server' that stores the output of the 'createServer' method
const server = http.createServer((request, response)=>{

	//console.log(request.url)//contains the URL endpoint

		// Accessing the 'greeting' route returns a message of "Welcome to 248 2048 App"
	if(request.url == "/greeting"){
		response.writeHead(200,{"Content-Type" : "text/plain"})
		response.end("Welcome to 248 2048 App")
	}
	// Accessing the 'homepage' route returns a message of 'This is the homepage!'
	else if (request.url == "/homepage"){
		response.writeHead(200,{"Content-Type" : "text/plain"})
		response.end("This is the homepage!")
	}
	//MA
		//create an else condition that all other routes will return a message of "Page Not Available"

		// All other routes will return a message of 'Page not available'

	else {
		response.writeHead(404,{"Content-Type" : "text/plain"})
		response.end("Page Not Available")
	}

})
// Uses the "server" and "port" variables created above.
server.listen(port)

// When server is running, console will print the message:
console.log(`Server is now accessible at localhost:${port}`)