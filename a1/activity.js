//a. What directive is used by Node.js in loading the modules it needs?

	// require()

//b. What Node.js module contains a method for server creation?

	// HTTP

//c. What is the method of the http object responsible for creating a server using Node.js?

	// createServer()

//d. What method of the response object allows us to set status codes and content types?

	// response.writerHead()

//e. Where will console.log() output its contents when run in Node.js?

	// Console

//f. What property of the request object contains the address' endpoint?

	// URL