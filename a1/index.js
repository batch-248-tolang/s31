const http = require("http");

const port = 3000;

const server = http.createServer((request, response)=>{
	if(request.url == "/login"){
		response.writeHead(200,{"Content-Type" : "text/plain"});
		response.end("Welcome to the login page.");
	}else if (request.url == "/register"){
		response.writeHead(200,{"Content-Type" : "text/plain"})
		response.end("This page is still under construction. Go back to login page!")
	}else {
		response.writeHead(404,{"Content-Type" : "text/plain"})
		response.end("Error 404: Page not found")
	}

})

server.listen(port)

console.log(`Server is now accessible at localhost:${port}`)